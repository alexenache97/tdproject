import java.io.FileNotFoundException;
import java.io.IOException;


public class Main {
	
	public static void main(String[] args) throws IOException {
		 Lexer lexer = new Lexer("mFile.txt");

	        while (!lexer.isExausthed()) {
	            lexer.moveAhead();
	        }

	        if (lexer.isSuccessful()) {
	            System.out.println("Ok!");
	        } else {
	            System.out.println(lexer.errorMessage());
	        }
	    }
}

}
