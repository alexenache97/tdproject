import java.util.regex.Matcher;
import java.util.regex.Pattern;

public enum Token {
    IF_KEYWORD ("if"),
    WHILE_KEYWORD ("while"),
    ELSE_KEYWORD ("else"),
    FUN_KEYWORD("fun"),
    VARIABLE(":"),
    REAL_VAR("real"),
    BOOL_VAR("bool"),
    
    SEMICOLON (";"),
    COMMA (","),
    COMMENT("%"),
    
    OPEN_SQUARE_PARENTHESIS("\\["),
    CLOSE_SQUARE_PARENTHESIS("\\]"),
    OPEN_BRACKET ("\\{"),
    CLOSE_BRACKET ("\\}"),
    OPEN_PARENTHESIS ("\\("),
    CLOSE_PARENTHESIS ("\\)"),
    EQUAL ("="),
    DIFFERENT ("<>"),
    GREATER_EQUAL (">="),
    LESSER_EQUAL ("<="),
    ATTRIBUTION (":="),
    GREATER (">"),
    LESSER ("<"),
    
    
    
    ADDITION ("\\+"),
    SUBTRACTION ("-"),
    MULTIPLICATION ("\\*"),
    DIVISION ("/"),
    

    STRING ("\"[^\"]+\""),
    NUMBER ("\\d+(\\.\\d+)?"),
    IDENTIFIER ("\\w+");    

    private final Pattern pattern;

    Token(String regex) {
        pattern = Pattern.compile("^" + regex);
    }
    int endOfMatch(String s) {
        Matcher m = pattern.matcher(s);

        if (m.find()) {
            return m.end();
        }

        return -1;
    }
   
}